﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TangoApp
{
    public partial class SelectHospital : ContentPage
    {
        List<string> hospitals = new List<string> { "Andhra Hospitals", "Kamineni Hospital", "Liberty Hospitals", "Manipal Super Speciality Hospital", "Ramesh Hospitals", "Sentini Hospitals", "Srikara Hospitals", "Vijaya Super Speciality Hospital" };
        public SelectHospital()
        {
            InitializeComponent();
            foreach (string temp in hospitals)
            {
                hospPicker.Items.Add(temp);
            }
        }
        async void goHospSearchClicked(object Sender, EventArgs e)
        {
            if(hospPicker.SelectedIndex == -1)
            {
                await DisplayAlert("No selection made", "Please make a selection", "OK");
            }
            else
            {
                string s = hospitals.ElementAt(hospPicker.SelectedIndex);
                await Navigation.PushAsync(new HospitalDetails(s));
            }
        }
    }
}
