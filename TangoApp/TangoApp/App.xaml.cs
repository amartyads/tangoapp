﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace TangoApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // MainPage = new TangoApp.MainPage();
            MainPage = GetMainPage();
        }
        public static Page GetMainPage()
        {
            var mainNav = new NavigationPage(new TangoApp.MainPage());
            return mainNav;
        }
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
