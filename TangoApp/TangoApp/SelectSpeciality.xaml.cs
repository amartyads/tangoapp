﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TangoApp
{
    public partial class SelectSpeciality : ContentPage
    {
        List<string> specialities = new List<string> { "Anaesthesiology", "Andrology", "Cardiology", "Dermatology", "ENT", "Gastrology", "General Medicine", "Gynaecology", "Hepatology", "Nephrology", "Neurology", "Oncology", "Orthopaedics", "Paediatrics", "Physician", "Physiotherapy", "Pulmonology", "Radiology", "Urology" };
        public SelectSpeciality()
        {
            InitializeComponent();
            foreach (string temp in specialities)
            {
                specPicker.Items.Add(temp);
            }
        }
        async void goSpecSearchClicked(object Sender, EventArgs e)
        {
            if (specPicker.SelectedIndex == -1)
            {
                await DisplayAlert("No selection made", "Please make a selection", "OK");
            }
            else
            {
                string s = specialities.ElementAt(specPicker.SelectedIndex);
                //check.Text = s;
                await Navigation.PushAsync(new SpecialityDoctors(s));
            }
        }
    }
}
