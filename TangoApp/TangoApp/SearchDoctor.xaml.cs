﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TangoApp
{
    public partial class SearchDoctor : ContentPage
    {
        public SearchDoctor()
        {
            InitializeComponent();
        }
        async void docSearchClicked(object Sender, EventArgs e)
        {
            if(doctorNameEntry.Text == null || doctorNameEntry.Text == "")
            {
                await DisplayAlert("No name entered", "Please enter a name", "OK");
            }
        }
    }
}
