﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TangoApp
{
    public partial class HospitalDetails : ContentPage
    {
        private string curHospital;

        public HospitalDetails()
        {
            InitializeComponent();
        }

        public HospitalDetails(string s)
        {
            curHospital = s;
            this.Title = curHospital;
            //await PopulatePage();
        }
        
    }
}
