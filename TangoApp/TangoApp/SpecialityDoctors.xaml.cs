﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TangoApp
{
    public partial class SpecialityDoctors : ContentPage
    {
        private string curSpeciality;

        public SpecialityDoctors()
        {
            InitializeComponent();
        }

        public SpecialityDoctors(string s)
        {
            InitializeComponent();
            curSpeciality = s;
            this.Title = curSpeciality;
        }
    }
}
